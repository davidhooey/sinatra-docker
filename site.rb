require 'sinatra'

module App

    class Site < Sinatra::Base

        set :bind, '0.0.0.0'
        set :static, true
        set :public_dir, File.expand_path(__dir__)

        get '/' do
            erb :'/index'
        end

    end

end
