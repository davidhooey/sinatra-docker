require_relative 'site'

module App

    class App

        def initialize(args)
            # Do some stuff before launching site.

            # Launch site
            Site.run!
        end

    end

end

App::App.new(ARGV)
